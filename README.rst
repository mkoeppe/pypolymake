pypolymake
==========

What is it?
-----------

The Python module pypolymake provides wrappers for
`Polymake <https://polymake.org/doku.php>`_.
It needs a not too recent enough version of polymake (tested with polymake
3.6 known to not work with polymake 4.X). It is only tested with Python 3.
When used inside `SageMath <http://www.sagemath.org/>`_ you have access to
special converters from pypolymake objects to SageMath objects.

The language has been kept as close as possible as the original perl
interface of Polymake. The following polymake construction::

    polytope> $c = cube(5);
    polytope> print $c->F_VECTOR;
    32 80 80 40 10
    polytope> $g = $c->GRAPH;
    polytope> print $g->DIAMETER;
    5

is translated in Python as::

    >>> from polymake import *
    >>> c = cube(5)
    >>> print(c.F_VECTOR)
    (32, 80, 80, 40, 10)
    >>> g = c.GRAPH
    >>> print(g.DIAMETER)
    5

As pypolymake is under development, many things are not working! Do not hesitate
to make a bug report on the `development page <https://gitlab.com/videlec/pypolymake>`_.

Installation and source code
----------------------------

The pypolymake package is available on PyPI, see
https://pypi.org/project/pypolymake/. To install
it within your system Python, just run the following
command::

    $ pip install pypolymake [--user] [--upgrade]

To install polymake and pypolymake inside SageMath run::

    $ sage -i polymake
    $ sage -pip install pypolymake [--user] [--upgrade]

If polymake headers or libraries are installed in a non standard location you
may need to set the environment variables CFLAGS, CXXFLAGS, LDFLAGS. For example,
on OS X, you need to update LDFLAGS as::

    $ export LDFLAGS="-L/System/Library/Perl/5.18/darwin-thread-multi-2level/CORE/ $LDFLAGS"

For SageMath users
------------------

If pypolymake is used inside SageMath then conversion from and to SageMath objects
are available. Most polymake objects have a `.sage()` method and you can use
`polymake.polymake` as a generic convertor. For example::

    sage: import polymake
    sage: p = polymake.polymake(polytopes.dodecahedron())
    sage: p
    Polytope<QuadraticExtension<Rational>>
    sage: p.VERTICES
    1 3-1r5 3-1r5 3-1r5
    1 -1+1r5 -4+2r5 0
    ...
    1 -3+1r5 -3+1r5 -3+1r5
    1 1-1r5 4-2r5 0
    sage: print(p.VERTICES.sage())
    [           1   -sqrt5 + 3   -sqrt5 + 3   -sqrt5 + 3]
    [           1    sqrt5 - 1  2*sqrt5 - 4            0]
    ...
    [           1    sqrt5 - 3    sqrt5 - 3    sqrt5 - 3]
    [           1   -sqrt5 + 1 -2*sqrt5 + 4            0]

Warning: the cohabitation between sage and polymake is not perfect. If you do
use polymake in sage *always* import sage first and then polymake. More precisely,
if you launch a simple python console and do 'import polymake' followed by
'import sage.all' it is most likely to end up with a (not yet identified)
SEGFAULT.

License
-------

The library pypolymake is distributed under the terms of the GNU General Public
License (GPL) as published by the Free Software Foundation; either version 3 of
the License, or (at your option) any later version (see http://www.gnu.org/licenses/)

Authors
-------

This project has been started by Burcin Erocal in 2011 and continued
by Vincent Delecroix since 2016.
